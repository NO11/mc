minetest.register_decoration({
    deco_type = "simple",
    place_on = {"mcl_nether:netherrack"},
    sidelen = 16,
    fill_ratio = 1,
    biomes = {"Nether"},
    y_max = mcl_vars.mg_nether_max,
    y_min = mcl_vars.mg_nether_min,
    decoration = "mc:warped_fungus" -- TODO: Add Warped and Crimson Forest and place fungus in correct biomes
})
minetest.register_decoration({
     deco_type = "simple",
     place_on = {"mcl_core:dirt_with_grass"},
     sidelen = 16,
     fill_ratio = 1,
     biomes = {"Taiga","Forest"},
     y_max = mcl_vars.mg_overworld_max,
     y_min = mcl_vars.mg_overworld_min,
     decoration = "mc:sweet_berry_bush_2"
 })
 if minetest.settings:get_bool("mcl_generate_ores", true) then
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = "mc:nether_gold_ore",
        wherein         = {"mcl_nether:netherrack", "mcl_core:stone"},
        clust_scarcity = 850,
        clust_num_ores = 4, -- MC cluster amount: 4-10
        clust_size     = 3,
        y_min = mcl_vars.mg_nether_min,
        y_max = mcl_vars.mg_nether_max,
    })
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = "mc:nether_gold_ore",
        wherein         = {"mcl_nether:netherrack", "mcl_core:stone"},
        clust_scarcity = 1650,
        clust_num_ores = 8, -- MC cluster amount: 4-10
        clust_size     = 4,
        y_min = mcl_vars.mg_nether_min,
        y_max = mcl_vars.mg_nether_max,
    })
end