minetest.register_node("mc:warped_fungus", {
    drawtype = "plantlike",
    tiles = {"warped_fungus.png"}
})
minetest.register_node("mc:sweet_berry_bush_0", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_0.png"},
    damage_per_second = 1
})
minetest.register_node("mc:sweet_berry_bush_1", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_1.png"},
    damage_per_second = 1
})
minetest.register_node("mc:sweet_berry_bush_2", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_2.png"},
    damage_per_second = 2,
    drop = "mc:sweet_berry 2"
})
minetest.register_node("mc:sweet_berry_bush_3", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_3.png"},
    damage_per_second = 2,
    drop = "mc:sweet_berry 3"
})
minetest.register_node("mc:nether_gold_ore", {
	description = ("Nether Gold Ore"),
	_doc_items_longdesc = ("Nether gold ore is an ore containing nether gold. It is commonly found around netherrack in the Nether."),
	stack_max = 64,
	tiles = {"mcl_nether_netherrack.png^mcl_nether_gold_ore.png"},
	is_ground_content = true,
	groups = {pickaxey=1, building_block=1, material_stone=1, xp=0},
	drop = {
		max_items = 1,
		items = {
			{items = {'mcl_core:gold_nugget 6'},rarity = 5},
			{items = {'mcl_core:gold_nugget 5'},rarity = 5},
			{items = {'mcl_core:gold_nugget 4'},rarity = 5},
			{items = {'mcl_core:gold_nugget 3'},rarity = 5},
			{items = {'mcl_core:gold_nugget 2'}},
		}
	},
	sounds = mcl_sounds.node_sound_stone_defaults(),
	_mcl_blast_resistance = 3,
	_mcl_hardness = 3,
	_mcl_silk_touch_drop = true,
	_mcl_fortune_drop = mcl_core.fortune_drop_ore
})