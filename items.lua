minetest.register_craftitem("mc:warped_fungus_stick", {
    description = "Warped Fungus on a Stick",
    inventory_image = "warped_fungus_stick.png"
})
minetest.register_craftitem("mc:sweet_berry", {
    description = "Sweet Berry",
    inventory_image = "sweet_berry.png",
    on_use = minetest.item_eat(2)
})