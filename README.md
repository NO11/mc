# mc

A upgrade pack that turns MineClone2 into Minecraft 1.16!

# Install

Simply uncompress the zip file, rename the folder inside to mc, put it in the mods directory, and enable the mod for the world, and you're good to go!

# What is added

* Warped Fungus
* Warped Fungus on a Stick

# Goals

* Piglins
* Dolphins
* Shipwrecks
* Netherite
* The new Nether biomes
* Drowned
* Tridents
* Bees
* Pandas
* Bamboo
* and many more things, but those are my main focus for now....

# TODO list:

* Netherite
* Bees
* New nether biomes
* Shipwrecks
* Everything else in Goals

# Contributing

I will definitely accept anything in minecraft 1.16 (code and textures), and reject anything that isnt. Simple. (i.e. anything in 1.17 is rejected, i hate 1.17)

If you want to contribute textures, they have to be exactly like the Minecraft ones, just redesigned as exact as possible.

# Hope you enjoy!