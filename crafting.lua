minetest.register_craft({
    type = "shapeless",
    output = "mc:warped_fungus_stick",
    recipe = {"mcl_fishing:fishing_rod", "mc:warped_fungus"}
})
minetest.register_craft({
	type = "cooking",
	output = 'mcl_core:gold_ingot',
	recipe = 'mc:nether_gold_ore',
	cooktime = 10,
})